       IDENTIFICATION DIVISION. 
       PROGRAM-ID. CONVRE3.
       AUTHOR. PASIN-RWRK.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT 100-INPUT-FILE ASSIGN TO  "DATA3.DAT"
              ORGANIZATION IS LINE SEQUENTIAL
              FILE STATUS IS WS-INPUTFILE-STATUS.
           SELECT 200-OUTPUT-FILE ASSIGN TO  "REPORT3.DAT"
              ORGANIZATION IS LINE SEQUENTIAL
              FILE STATUS IS WS-OUTPUTFILE-STATUS.

       DATA DIVISION. 
       FILE SECTION. 
       FD 100-INPUT-FILE
           BLOCK CONTAINS 0 RECORDS .
       01 INPUT-FILE-RECORD.
           05 BRANCH-ID         PIC X.
           05 FILLER            PIC X(6).
           05 DATE-DMY          PIC X(8).
           05 FILLER            PIC X(3).
           05 PRODUCT           PIC X(9). 
           05 INCOME            PIC 9(3).
       FD 200-OUTPUT-FILE
           BLOCK CONTAINS 0 RECORDS .
       01 OUTPUT-FILE-RECORD    PIC X(80).

       WORKING-STORAGE SECTION. 
       01 WS-INPUTFILE-STATUS  PIC X(2).
           88 FILE-OK           VALUE "00".
           88 FILE-AT-END       VALUE "10".
       01 WS-OUTPUTFILE-STATUS  PIC X(2).
           88 FILE-OK           VALUE "00".
           88 FILE-AT-END       VALUE "10".
       01 WS-CALCULATION.
           05 WS-COUNT-READ      PIC 9(5).
           05 WS-ALL-TOTAL       PIC 9(5) VALUE ZEROS.
           05 WS-BRANCH-TOTAL    PIC 9(5).
           05 WS-BRANCH-CB       PIC X.
           05 WS-BRANCH-SHOW     PIC X VALUE "S".
           05 WS-DATE-TOTAL      PIC 9(5).
           05 WS-DATE-CB         PIC X(8).
           05 WS-DATE-SHOW       PIC X VALUE "S".
           05 WS-PRODUCT-TOTAL   PIC 9(5).
           05 WS-PRODUCT-CB      PIC X(9).
       01  RPT-FORMAT.
           05 RPT-HEADER        PIC X(39)
              VALUE "BRANCH   DATE   PRODUCT          INCOME".
           05 RPT-DETAIL.
              10 RPT-BRANCH-ID  PIC X(1).
              10 FILLER         PIC X(6) VALUE SPACE.
              10 RPT-DATE       PIC X(8).
              10 FILLER         PIC X(2) VALUE SPACE.
              10 RPT-PRODUCT    PIC X(9).
              10 FILLER         PIC X(6) VALUE SPACE.
              10 RPT-TOTAL      PIC ZZZZZ9.
           05 RPT-BRANCH-TOTAL.
              10 FILLER         PIC X(32)
                 VALUE "----------BRANCH TOTAL----------".
              10 RPT-TOTAL      PIC ZZZZZ9.
           05 RPT-DATE-TOTAL.
              10 FILLER         PIC X(32)
                 VALUE "            DATE TOTAL          ".
              10 RPT-TOTAL      PIC ZZZZZ9.
           05 RPT-ALL-TOTAL.     
              10 FILLER         PIC X(32)
                 VALUE ">           ALL TOTAL          <".
              10 RPT-TOTAL      PIC ZZZZZ9.


       PROCEDURE DIVISION .
       0000-MAINPROGRAM.
           PERFORM 1000-INTIAL THRU 1000-EXIT
           PERFORM 2000-PROCESS THRU 2000-EXIT
              UNTIL FILE-AT-END OF WS-INPUTFILE-STATUS
           PERFORM 3000-END THRU 3000-EXIT
           GOBACK
           .
       
       1000-INTIAL.
           PERFORM 1100-OPEN-INPUT THRU 1100-EXIT 
           PERFORM 1200-OPEN-OUTPUT THRU 1200-EXIT   
           MOVE ZEROS TO WS-ALL-TOTAL 
           MOVE RPT-HEADER TO OUTPUT-FILE-RECORD 
           PERFORM 7000-WRITE THRU 7000-EXIT 
           PERFORM 8000-READ THRU 8000-EXIT
           .
       1000-EXIT.
           EXIT.
       1100-OPEN-INPUT.
           OPEN INPUT 100-INPUT-FILE 
           IF FILE-OK  OF WS-INPUTFILE-STATUS 
              CONTINUE
           ELSE
              DISPLAY "***** CONBRE3 ABEND *****"
                 UPON CONSOLE 
              DISPLAY "* PARA 1100-OPEN-INPUT *"
                 UPON CONSOLE
              DISPLAY "* FILE STATUS=" WS-INPUTFILE-STATUS 
                 UPON CONSOLE
              DISPLAY "***** CONBRE3 ABEND *****"
                 UPON CONSOLE
              STOP RUN
           END-IF 
           .
       1100-EXIT.
           EXIT.

       1200-OPEN-OUTPUT.
           OPEN OUTPUT 200-OUTPUT-FILE 
           IF FILE-OK  OF WS-OUTPUTFILE-STATUS 
              CONTINUE
           ELSE
              DISPLAY "***** CONBRE2 ABEND *****"
                 UPON CONSOLE 
              DISPLAY "* PARA 1200-OPEN-OUTPUT *"
                 UPON CONSOLE
              DISPLAY "* FILE STATUS=" WS-OUTPUTFILE-STATUS 
                 UPON CONSOLE
              DISPLAY "***** CONBRE2 ABEND *****"
                 UPON CONSOLE
              STOP RUN
           END-IF 
           .
       1200-EXIT.
           EXIT.
       2000-PROCESS.
           MOVE BRANCH-ID TO WS-BRANCH-CB
           MOVE ZEROS TO WS-BRANCH-TOTAL 
           PERFORM 2100-BRANCH-PROCESS THRU 2100-EXIT
              UNTIL FILE-AT-END OF WS-INPUTFILE-STATUS
              OR BRANCH-ID NOT= WS-BRANCH-CB
              
      *    DISPLAY BRANCH-ID " " WS-BRANCH-CB " " WS-BRANCH-TOTAL
           MOVE WS-BRANCH-TOTAL TO RPT-TOTAL OF RPT-BRANCH-TOTAL  
      *    DISPLAY RPT-BRANCH-TOTAL 
           MOVE RPT-BRANCH-TOTAL  TO OUTPUT-FILE-RECORD 
           PERFORM 7000-WRITE THRU 7000-EXIT 
           MOVE "S" TO WS-BRANCH-SHOW 
           .
       2000-EXIT.
           EXIT.

       2100-BRANCH-PROCESS.
           MOVE DATE-DMY TO WS-DATE-CB 
           MOVE ZEROS TO WS-DATE-TOTAL 
           PERFORM 2200-DATE-PROCESS THRU 2200-EXIT
              UNTIL FILE-AT-END OF WS-INPUTFILE-STATUS
              OR BRANCH-ID NOT= WS-BRANCH-CB
              OR DATE-DMY NOT= WS-DATE-CB
      *    DISPLAY DATE-DMY " " WS-DATE-CB " " WS-DATE-TOTAL
           MOVE WS-DATE-TOTAL TO RPT-TOTAL OF RPT-DATE-TOTAL  
           MOVE RPT-DATE-TOTAL  TO OUTPUT-FILE-RECORD 
           PERFORM 7000-WRITE THRU 7000-EXIT 
           MOVE "S" TO WS-DATE-SHOW 

           .
       2100-EXIT.
           EXIT.
       2200-DATE-PROCESS.
           MOVE PRODUCT TO WS-PRODUCT-CB 
           MOVE ZEROS TO WS-PRODUCT-TOTAL 
           PERFORM 2300-PRODUCT-PROCESS THRU 2300-EXIT
              UNTIL FILE-AT-END OF WS-INPUTFILE-STATUS
              OR BRANCH-ID NOT = WS-BRANCH-CB
              OR DATE-DMY NOT = WS-DATE-CB
              OR PRODUCT NOT = WS-PRODUCT-CB
      *    DISPLAY PRODUCT " " WS-PRODUCT-CB " " WS-PRODUCT-TOTAL
           IF WS-BRANCH-SHOW = "S"
              MOVE WS-BRANCH-CB TO RPT-BRANCH-ID  OF RPT-DETAIL 
              MOVE "H" TO WS-BRANCH-SHOW
           ELSE 
              MOVE SPACES    TO    RPT-BRANCH-ID  OF RPT-DETAIL 
           END-IF 
           IF WS-DATE-SHOW = "S"
              MOVE WS-DATE-CB      TO RPT-DATE OF RPT-DETAIL 
              MOVE "H" TO WS-BRANCH-SHOW
           ELSE 
              MOVE SPACE      TO RPT-DATE OF RPT-DETAIL  
           END-IF
           MOVE WS-PRODUCT-CB   TO RPT-PRODUCT OF RPT-DETAIL 
           MOVE WS-PRODUCT-TOTAL TO RPT-TOTAL OF RPT-DETAIL   
           MOVE RPT-DETAIL  TO OUTPUT-FILE-RECORD 
           PERFORM 7000-WRITE THRU 7000-EXIT
           .
       2200-EXIT.
           EXIT.

       2300-PRODUCT-PROCESS.
      *    DISPLAY BRANCH-ID " " DATE-DMY " " PRODUCT " " INCOME 
           ADD INCOME TO WS-ALL-TOTAL 
           ADD INCOME TO WS-BRANCH-TOTAL 
           ADD INCOME TO WS-DATE-TOTAL 
           ADD INCOME TO WS-PRODUCT-TOTAL
           PERFORM 8000-READ THRU 8000-EXIT
           .
       2300-EXIT.
           EXIT.

       3000-END.
      *    DISPLAY "ALL TOTAL : "WS-ALL-TOTAL 
           MOVE WS-ALL-TOTAL TO RPT-TOTAL OF RPT-ALL-TOTAL 
           MOVE RPT-ALL-TOTAL TO OUTPUT-FILE-RECORD 
           PERFORM 7000-WRITE THRU 7000-EXIT 
           CLOSE 100-INPUT-FILE 
           CLOSE 200-OUTPUT-FILE 
           DISPLAY "READ " WS-COUNT-READ " RECORDS"
           .
       3000-EXIT.
           EXIT.
       7000-WRITE.
           WRITE OUTPUT-FILE-RECORD 
           IF FILE-OK  OF WS-OUTPUTFILE-STATUS 
              CONTINUE
           ELSE
              DISPLAY "***** CONBRE3 ABEND *****"
                 UPON CONSOLE 
              DISPLAY "* PARA 7000-OPEN-OUTPUT *"
                 UPON CONSOLE
              DISPLAY "* FILE STATUS=" WS-OUTPUTFILE-STATUS 
                 UPON CONSOLE
              DISPLAY "***** CONBRE3 ABEND *****"
                 UPON CONSOLE
              STOP RUN
           END-IF
           .
       7000-EXIT.
           EXIT.

       8000-READ.
           READ 100-INPUT-FILE
           IF FILE-OK OF WS-INPUTFILE-STATUS 
              ADD 1 TO WS-COUNT-READ 
           ELSE
              IF FILE-AT-END OF WS-INPUTFILE-STATUS 
                 CONTINUE
              ELSE
                 DISPLAY "***** CONBRE3 ANEND *****"
                    UPON CONSOLE 
                 DISPLAY "* PARA 8000-READ FAIL *"
                    UPON CONSOLE 
                 DISPLAY "* FILE STATUS: " WS-INPUTFILE-STATUS " *"
                    UPON CONSOLE 
                 DISPLAY "***** CONBRE3 ANEND *****"
                    UPON CONSOLE 
                 STOP RUN 
              END-IF 
           END-IF

           .
       8000-EXIT.
           EXIT.
